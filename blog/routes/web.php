<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('orders','OrderController');
Route::get('/', function () {
    return view('welcome');
});
Route::get('foo', function () {
    return 'Hello World';
});

Route::get('posts/{post}/comments/{comment}', function ($postId, $commentId) {
   //
   return 'post:'.$postId.'comment id'.$commentId;
})->where('post','[0-9]+','comment','[A-Za-z]+');
Route::get('emp/{name?}', function ($banda = 'john') {
    return $banda;
})->where('name', '[A-Za-z]+');
Auth::routes();
